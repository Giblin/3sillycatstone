#!/usr/bin/sh
python -m virtualenv -v venv
source venv/bin/activate
pip install -r requirements.txt
export FLASK_APP=project.py
export FLASK_ENV=development
python -m flask --debug run

# source venv/Scripts/activate
# export FLASK_APP=project2.py
# export FLASK_ENV=development
# python -m flask run