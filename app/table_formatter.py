data = ""

print("Enter/Paste. Ctrl-D or Ctrl-Z ( windows ) to save it.")

while True:
    try:
        line = input()
    except EOFError:
        data = data[:-1]
        break
    
    data += line + "_"

numbers = data.split("_")
output = "INSERT INTO ()\nVALUES "

for i in range(1, len(numbers) + 1):
    output += f"({numbers[i - 1]}),"
    if (i % 5 == 0):
        output += "\n"

if (output[-1:] == "\n"):
    output = output[:-2] + ";"
else:
    output = output[:-1] + ";"

print(output)

''' TEST NUMS
132
3213
43
424
423
4242
423
43
432
54
678
43
'''