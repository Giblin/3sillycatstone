from flask_wtf import FlaskForm
from wtforms import IntegerField, DecimalField, SelectField, StringField, SubmitField, RadioField, BooleanField
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea

class MainForm(FlaskForm):
    num_rows = 1 # Dynamically generated with jinja in html
    categories = []
    categories_data = []

    # Only non dynamically generated field
    button_calc = SubmitField('Calculate')

class SugmaForm(FlaskForm):
    suggestion = StringField(widget=TextArea())
    button_submit = SubmitField('Submit')
    button_truncate = SubmitField('Truncate')
