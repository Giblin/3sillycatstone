from typing import Callable

STATES = []
STATES_BY_ID = {}
get_data_func = None

def set_state_data(i_get_data_func:Callable, i_states:list) -> None:
    ''' Populates relevant global variables for use in below methods '''
    global STATES
    global STATES_BY_ID
    global get_data_func

    get_data_func = i_get_data_func
    STATES = i_states
    STATES_BY_ID = dict((id, state) for id, state in STATES)

def generate_scorecard() -> dict:
    '''Grabs the states from the database and makes a 
    dictionary where scores will be added to'''
    state_tuples = (STATES) #TODO FIX THIS
    o_scorecard = dict((state, 0) for id, state in state_tuples)

    return o_scorecard

def sort_category(i_category:list, i_order:str='Good') -> list:
    '''Takes a list of tuples and sorts them based on 'Good' or 'Bad''' 
    sorted_category = []

    if i_order == 'Bad':
        sorted_category = sorted(i_category, key=lambda x:x[1])
    if i_order == 'Good':
        sorted_category = sorted(i_category, key=lambda x:x[1], reverse=True)
    
    return sorted_category

def score_a_category(i_scorecard:dict, i_category:list, i_weight:int) -> dict:
    '''Takes a category and adds respective scores based on the provided scoring weight'''
    category_formatted = dict((id, value) for id, value in i_category)
    rank_score = len(i_scorecard)

    for state in category_formatted:
        #match state id to a name to the scorecard can be added to
        state_name = STATES_BY_ID[state]
        #number to be addi3 scoring algorithm
        i_scorecard[state_name] += (rank_score * i_weight)
        rank_score -= 1
    
    scorecard_sorted = dict(sorted(i_scorecard.items()))
    return scorecard_sorted

def get_category_data(i_scorecard:dict, i_category:list) -> dict:
    '''Takes a category and obtains the relevant data'''
    category_formatted = dict((id, value) for id, value in i_category)

    for state in category_formatted:
        #match state id to a name to the scorecard can be added to
        state_name = STATES_BY_ID[state]
        #number to be added, ie, this is the scoring algorithm
        i_scorecard[state_name] += category_formatted[state]
    
    scorecard_sorted = dict(sorted(i_scorecard.items()))
    return scorecard_sorted

def rank_all_categories(i_scorecard:dict, i_sort_inputs:list) -> dict:
    '''Scores all categories and returns a dictionary with composite scores.
    Expects a list of of lists that look like [<category>, <good/bad>, <weight>]'''
    for dataset in i_sort_inputs:
        cat_data = sort_category(dataset[0], dataset[1])
        i_scorecard = score_a_category(i_scorecard, cat_data, dataset[2])
    
    return i_scorecard

def rank_one_category(i_scorecard:dict, i_sort_inputs:list) -> dict:
    '''Scores one category and return a dictionary with scores.
    Expects a list that looks like [<category>, <good/bad>, <weight>]'''
    cat_data = sort_category(i_sort_inputs[0], i_sort_inputs[1])
    i_scorecard = score_a_category(i_scorecard, cat_data, i_sort_inputs[2])

    return i_scorecard

if __name__ == '__main__':
    generate_scorecard()