from dataclasses import dataclass
from app import app
from app.forms import MainForm, SugmaForm
from flask import render_template, request
import json
from app.data_processing import Database, DB_PATH, SUG_DB_PATH
import app.state_ranker as StateRanker
import app.input_processing as Input

@app.route('/', methods=['GET', 'POST'])
def main():
    # Form Variables
    categories = Database.get_categories()
    NUM_CATEGORY_ROWS = 10
    NULL_CATEGORY = '---'

    # Data Variables
    StateRanker.set_state_data(Database.get_table, Database.get_states())

    # Preapply variables to display correctly
    form = MainForm()
    form.num_rows = NUM_CATEGORY_ROWS
    form.categories = categories
    
    categories_data = [] # Saves form so it can be reapplied after POST
    
    user_scorecard = {} # Tables that will be display to the user

    if request.method == 'POST':
        all_row_inputs = []
        
        # category_row_inputs is the list of lists that is used to generate the composite score
        for i in range(NUM_CATEGORY_ROWS):
            # Get data from user's input
            cat_name = request.form.get(f'dropDown_{i}')
            if (cat_name == NULL_CATEGORY):
                break
            cat_dir = Input.format_checkbox(request.form.get(f'stat_dir_{i}'))
            cat_weight = request.form.get(f'radio_field_{i}')

            # Save category data
            row_data = []

            row_data.append(cat_name)
            if cat_dir == "Good":
                row_data.append(True)
            else:
                row_data.append(False)
            row_data.append(int(cat_weight))

            categories_data.append(row_data)

            # Throw data into list of lists
            row_inputs = []
            all_row_inputs.append(row_inputs)
            
            row_inputs.append(Database.get_table(DB_PATH, Input.format_category_name(cat_name))) # replace spaces with underscores
            row_inputs.append(Input.format_checkbox(cat_dir))
            row_inputs.append(int(cat_weight))

        # category_row_inputs = Input.validate_rows(NULL_CATEGORY, category_row_inputs) # removes null items
        user_scorecard = StateRanker.rank_all_categories(StateRanker.generate_scorecard(), all_row_inputs) # generate scorecard
        sorted_user_scorecard = dict(sorted(user_scorecard.items(), key=lambda x:x[1], reverse=True)) # sort scorecard

        form.categories_data = categories_data

        return render_template('main.html', form=form, num_rows=NUM_CATEGORY_ROWS, categories=categories, categories_data=categories_data, user_scorecard=sorted_user_scorecard)

    if request.method == 'GET':
        row_data = []
        
        row_data.append(categories[1])
        row_data.append(False)
        row_data.append(1)

        categories_data.append(row_data)

        form.categories_data = categories_data

        return render_template('main.html', form=form, num_rows=NUM_CATEGORY_ROWS, categories=categories, categories_data=categories_data, user_scorecard=user_scorecard)

@app.route('/sug', methods=['GET', 'POST'])
def main_sug():
    ''' Sugma form HAHA Got'em '''
    form = SugmaForm()
    if request.method == 'POST':
        try:
            if request.form['button_submit'] == 'Submit':
                Database.add_suggestion((request.form.get('suggestion')))
        except:
            pass
        
        try:
            if request.form['button_truncate'] == 'Truncate':
                Database.delete_all_suggestion()
        except:
            pass

        return render_template('suggestions.html', form=form)

    if request.method == 'GET':
        return render_template('suggestions.html', form=form)