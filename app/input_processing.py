# While trying to remove dependancy issues this file has a lot of issues,
# it should only deal with input and should not use state_ranker
# the dump_table function should just be in data_processing
# this whole file should probably be split up into other files

from app import app
from decimal import Decimal

import app.state_ranker as StateRanker
from app.data_processing import Database


def format_checkbox(input:str) -> str:
    '''Converts None to 'Bad' '''
    if input == None:
        new_input = 'Bad'
        return new_input
    else:
        return input

def format_category_name(input:str) -> str:
    '''Replace spaces with underscores for SQL input'''
    if type(input) == str:
        new_input = input.replace(" ", "_")
        return new_input 

def validate_rows(invalid_input:str, rows:list) -> list:
    '''Takes in a list of input rows, then marks and deletes all rows that contain null or invalid inputs'''
    invalid_rows = [] # a list of rows that contain invalid inputs
    for row in range(len(rows)):
        if invalid_input in rows[row]:
            invalid_rows.append(row)
        elif None in rows[row]:
            invalid_rows.append(row)
    for invalid_row in reversed(invalid_rows): #iterates in reverse to avoid indexing issues
        del rows[invalid_row]
    return rows