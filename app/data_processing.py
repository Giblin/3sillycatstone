import sqlite3 as sql

DB_PATH = "statestats.db"
SUG_DB_PATH = "suggestions.db"
SUG_TABLE = 'Suggestions'
SUG_COLUMN = 'Suggestion_String'


SQL_GET_CATEGORIES = "SELECT name FROM sqlite_master WHERE type='table' AND name != 'sqlite_sequence' ORDER BY name ASC;"
SQL_GET_TABLE = "SELECT * FROM {};"
SQL_ADD_SUGGESTION = 'INSERT INTO Suggestions(Suggestion_String) VALUES ("{}");' # Format with <table>, <column>, <value>
SQL_DELETE_ALL_SUGGESTION = 'DELETE FROM Suggestions'

class Database:
    ''' Class meant to not have instances which contains a sigular reference to the database
        and all functions to interact with it '''
    
    @classmethod
    def _run_sql(cls, database:str=DB_PATH, *i_sql_commands:str):
        ''' - Opens a temporary connection with the database and runs an sql command
            - (do not call outside of this file) '''
        o_data = []
        db_con = None

        try:
            db_con = sql.connect(database)
            
            for sql_command in i_sql_commands:
                o_data.extend(db_con.execute(sql_command).fetchall())
            
            db_con.commit()
        except Exception as e:
            print(f"An exception has occurred: {e}")
            db_con.rollback()
        else:
            db_con.commit() # this 'saves' changes to our database, same as the Write button does in DB Browser
            return o_data
        finally:
            if db_con != None: # this will make sure we close our connection if it has not already been closed - some statements like the 'with' keyword in python implicitly run __close__() at the end of the indented blocks
                db_con.close()

    # Public Functions
    @classmethod
    def get_table(cls, db_path, i_table_name:str) -> list:
        ''' Pulls all SQL data from a table via the table's name'''
        o_dict = cls._run_sql(db_path, SQL_GET_TABLE.format(i_table_name))

        if (o_dict == None):
            return None
        
        return list(o_dict)
    
    @classmethod
    def add_suggestion(cls, input:str) -> None:
        cls._run_sql(SUG_DB_PATH, SQL_ADD_SUGGESTION.format(input))

    @classmethod
    def delete_all_suggestion(cls) -> None:
        ''' Delete all '''
        cls._run_sql(SUG_DB_PATH, SQL_DELETE_ALL_SUGGESTION)

    @classmethod
    def get_states(cls) -> list:
        ''' Just returns states as a list '''
        return cls.get_table(DB_PATH, "States")

    @staticmethod
    def format_table_title_underscore(i_data_dict:list) -> list:
        ''' Replace spaces with _s '''
        return [i.replace(" ", "_") for i in i_data_dict] # Set spaces to underscores

    @staticmethod
    def format_table_title_spaces(i_data_dict:list) -> list:
        ''' Replace _s with spaces '''
        return [i.replace("_", " ") for i in i_data_dict] # Set underscores to spaces

    @classmethod
    def get_categories(cls) -> list:
        ''' - Gets the categories which is all tables except for States
            - Formats categories with spaces instead of underscores
            - Adds a null category at the top '''
        o_categories = cls._run_sql(DB_PATH, SQL_GET_CATEGORIES)

        for i in range(len(o_categories)):
            o_categories[i] = o_categories[i][0]

        o_categories.remove("States")
        o_categories.insert(0, "---")
        o_categories = Database.format_table_title_spaces(o_categories)
        
        return o_categories

if __name__ == "__main__":
    insert_query = SQL_ADD_SUGGESTION.format(SUG_TABLE, SUG_COLUMN, 'no')
    delete_query = SQL_DELETE_ALL_SUGGESTION.format(SUG_TABLE)
    Database._run_sql(SUG_DB_PATH, insert_query)
    print(Database.get_table(SUG_DB_PATH, 'Suggestions'))
    Database._run_sql(SUG_DB_PATH, delete_query)
    print('Truncated:')
    print(Database.get_table(SUG_DB_PATH, 'Suggestions'))
    