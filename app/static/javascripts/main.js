const CATEGORY_NULL_SELECTION = "---";

// Gets all select elements as a HTML collection, then converts to an array, then maps array with just ids
const DROPDOWN_IDS = [].slice.call(document.getElementsByTagName('select')).map(dropDown => dropDown.id);

let categories = []; //Decided at startup, a list of all categories (doesn't change)
let usedCategories = []; //Running list of used categories (added to & removed from when a drop down category is selected)

// #region ---=== STARTUP ===---
/**All basic one-time setup for when the page first opens */
function startup()
{
    let baseOptions = document.getElementById(DROPDOWN_IDS[0]).options;
    for (let i = 0; i < baseOptions.length; i++)
        categories.push(baseOptions[i].text);

    //Makes sure 0 is updated
    updateDropDown(0);

    //Adds events to drop downs
    for (let i = 0; i < DROPDOWN_IDS.length; i++)
    {
        // General update is called every time they are changed
        document.getElementById(DROPDOWN_IDS[i]).addEventListener("change", function() { updateDropDown(i) });
        // Saves the old value before an update happens (also has to call blur to force deselect the box to keep track of changes step by step)
        document.getElementById(DROPDOWN_IDS[i]).addEventListener("focus", function() { this.oldValue = this.value; this.blur(); });
    }
}

startup();
// #endregion --- STARTUP ---


// #region ---=== DROP DOWN LOGIC ===---
/**Gets drop down (select) element by index
 * @param {int} i_id index */
function getDropDown(i_id)
{
    return document.getElementById(DROPDOWN_IDS[i_id]);
}

/**Adds to a global list of used categories
 * @param {string} i_value */
function addUsedCategoriesValue(i_value)
{
    if (!usedCategories.includes(i_value))
        usedCategories.push(i_value)
}

/**Removes from a global list of used categories
 * @param {string} i_value */
function removeUsedCategoriesValue(i_value)
{
    if (usedCategories.includes(i_value))
        usedCategories.splice(usedCategories.indexOf(i_value), 1);
}

/**Set the disabled attribute of drop down elements & sets value to null if disabled
 * @param {int} i_id index 
 * @param {boolean} i_disabled true == disabled*/
function setDropDownDisabled(i_id, i_disabled)
{
    getDropDown(i_id).disabled = i_disabled;

    if (i_disabled)
    {
        getDropDown(i_id).oldValue = getDropDown(i_id).value;
        getDropDown(i_id).value = CATEGORY_NULL_SELECTION;
        updateDropDown(i_id);
    }
}

/**- Remove all existing options and remakes them while excluding the usedCategories
 * - Call anytime any value is added or remove from usedCategories */
function updateAllDropDownOptions()
{
    for (let i = 0; i < DROPDOWN_IDS.length; i++)
    {
        dropDown = document.getElementById(DROPDOWN_IDS[i]);
        options = dropDown.options;
        currentValue = dropDown.value;
        
        // Remove all existing options
        dropDown.options.length = 0;
    
        // Recreate options except and only include used options for the dropdown that is using it
        for (var j = 0; j < categories.length; j++)
        {
            if (usedCategories.includes(categories[j])) // If the current category is in usedCategories
                if (currentValue != categories[j]) // If the current category is is not being used for this drop down
                    continue;
            
            let newOption = document.createElement('option');
            newOption.text = categories[j];
            dropDown.appendChild(newOption); //add() also work
        }

        // Should already be in list but when the page refreshes it wasn't being added properly
        if (currentValue != CATEGORY_NULL_SELECTION)
            addUsedCategoriesValue(currentValue);

        dropDown.value = currentValue;
    }
}

/**Reset row to default state
 * @param {int} i_id index */
function clearRow(i_id)
{
    document.getElementById(`dropDown_${i_id}`).value = CATEGORY_NULL_SELECTION;
    document.getElementById(`stat_dir_${i_id}`).checked = false;
    document.getElementsByName(`radio_field_${i_id}`)[0].checked = true;
}

/**Makes sure a dropdown's state is updated after it's value is changed
 * It is changed in the html and updated here after because of an EventListener for the "change" event
 * @param {int} i_id index */
function updateDropDown(i_id)
{
    let thisElem = getDropDown(i_id);
    let thisValue = thisElem.value;
    let thisOldValue = thisElem.oldValue;

    if (thisValue == thisOldValue) //Don't run if nothing changed
        return;

    // If new selection of the category dropdown is the null value
    if (thisValue == CATEGORY_NULL_SELECTION)
    {
        removeUsedCategoriesValue(thisOldValue);

        // Moves all values up to null is not above any actual values
        for (let i = i_id; i < DROPDOWN_IDS.length - 1; i++) // Starts at current dropdown to 1 before last
        {
            // Drop Downs
            let curDropDown = getDropDown(i);
            let nextDropDown = getDropDown(i + 1);
            let lastLoop = false;
            if (nextDropDown.value == CATEGORY_NULL_SELECTION && curDropDown.value == CATEGORY_NULL_SELECTION)
            {
                setDropDownDisabled(i + 1, true);
                lastLoop = true;
            }

            curDropDown.innerHTML = nextDropDown.innerHTML; // Has to take the options of the next value to set to its value
            curDropDown.value = nextDropDown.value; // Set cur value to next value
            nextDropDown.value = CATEGORY_NULL_SELECTION; // Set next value to null

            // Direction Toggle
            curDir = document.getElementById(`stat_dir_${i}`);
            nextDir = document.getElementById(`stat_dir_${i + 1}`);

            curDir.checked = nextDir.checked;
            nextDir.checked = false;

            // Radio Buttons
            curSelected = document.querySelector(`input[name="radio_field_${i}"]:checked`).value;
            nextSelected = document.querySelector(`input[name="radio_field_${i + 1}"]:checked`).value;
            curRadios = document.getElementsByName(`radio_field_${i}`)
            nextRadios = document.getElementsByName(`radio_field_${i + 1}`)

            curRadios[nextSelected - 1].checked = true;
            nextRadios[0].checked = true;

            if (lastLoop)
                break;
        }

        if (i_id == DROPDOWN_IDS.length - 1)
        {
            clearRow(i_id);
        }

        updateAllDropDownOptions();
    }
    else // Anything but the null input
    {
        // Record that this new category is now being used
        addUsedCategoriesValue(thisValue);
        // Because this drop down has been updated the old value is no longer being used
        removeUsedCategoriesValue(thisOldValue);
        // Update the drop down options so that used values do not appear
        updateAllDropDownOptions();

        if (i_id < DROPDOWN_IDS.length - 1) // If not end of the category selections...
            setDropDownDisabled(i_id + 1, false); // ...enable lower dropdown
    }
}

updateAllDropDownOptions() // Set on refresh
// #endregion --- DROP DOWN LOGIC ---


// #region ---=== CAT BUTTON LOGIC ===---
const URL_LIST = ["https://media.tenor.com/71bZdJOKqqgAAAAM/spideyvivi.gif",
"https://c.tenor.com/eWtKnIVeADEAAAAj/cat-explode.gif",
"https://t1.daumcdn.net/news/202105/25/kakaopage/20210525064807474cnsk.gif",
"https://images.gr-assets.com/hostedimages/1538587233ra/26396005.gif",
"https://4.bp.blogspot.com/-7SDpQHKWosE/Vnrrvtfp3LI/AAAAAAABid8/T94Jfq9Iog4/s1600/funny-cat-gifs-186-06.gif",
"https://media.tenor.com/images/a697aa48bca83223bdf9879d9ede0051/tenor.gif",
"https://1.bp.blogspot.com/-gD8ZTF9FZWU/URzZl-kK-fI/AAAAAAAAHvw/iX9f9UnEBVM/s280/surprise+people.gif",
"https://media1.tenor.com/images/f864dae28a529458d07b6ef3a9a62ead/tenor.gif?itemid=3528989",
"https://uploads.disquscdn.com/images/8ef8b7e4a31815d0d5b7373d3a9a19e6210781fae803ee37327e81e334f4e874.gif"];

function catRandomizer()
{
    gifBox = document.getElementById("gif-box");
    urlIndex = Math.floor(Math.random() * URL_LIST.length);
    chosenOne = URL_LIST[urlIndex];

    if (chosenOne == gifBox.style.backgroundImage) // No repeat
        chosenOne = URL_LIST[urlIndex + 1 % URL_LIST.length];    

    gifBox.style.backgroundImage = `url(${chosenOne})`;
}

document.getElementById("cat_button").addEventListener('click', function() { catRandomizer() });
// #endregion --- CAT BUTTON LOGIC ---

