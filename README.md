# 3SillyCatstone

## Name
Personal State Ranker Capstone Project (Air Force Tech School)

## Description
Click on cool categories and get cool results. + cats

## Sources
Categories:
- Violent Crime Rates (per 100k  People): https://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_violent_crime_rate
- Cost of Living: https://worldpopulationreview.com/state-rankings/cost-of-living-index-by-state
- Poverty Rate: https://worldpopulationreview.com/state-rankings/poverty-rate-by-state
- Minimum Wage: https://en.wikipedia.org/wiki/List_of_US_states_by_minimum_wage
- Number of State Parks: https://www.smallcarcamp.com/how-many-state-parks-are-there-in-every-state/#:~:text=Here%E2%80%99s%20How%20Many%20State%20Parks%20Are%20There%20In,7%20Colorado%20...%208%20Connecticut%20...%20More%20items
- Income Tax Percent: https://en.wikipedia.org/wiki/State_income_tax
- Walmart Employees Per 100,000: https://247wallst.com/special-report/2021/01/17/how-many-people-work-at-walmart-in-each-state-and-what-they-are-paid-2/
- Number of McD's: https://247wallst.com/special-report/2021/11/25/states-with-the-most-mcdonalds-restaurants/
- Yearly Rain/Snow in Inches: https://www.netstate.com/states/tables/state_precipitation.htm#:~:text=Average%20yearly%20precipitation%20by%20state.%20%28List%20by%20state,%2856%20centimeters%29%206.%20Colorado%3A%2015%20inches%20%2838%20
- Health (Obesity Percent): https://worldpopulationreview.com/state-rankings/most-obese-states
- Percent of Depression: https://worldpopulationreview.com/state-rankings/most-depressed-states
- Total Population: https://worldpopulationreview.com/states
- Population Growth Rate: https://worldpopulationreview.com/states
- GDP Per Capita: https://worldpopulationreview.com/state-rankings/gdp-by-state
- Green State Score: https://worldpopulationreview.com/state-rankings/greenest-states

Raw Google Sheet with ALL the data: https://docs.google.com/spreadsheets/d/1U08XeYrjqKKkHr3ZKYs83I_7Ia8Nzt_Aao4bt3qgwq8/edit?usp=sharing
(Please pay no attention to the link names :P)


## Visuals
https://cdn3.emoji.gg/emojis/6276-spinning-cat.gif

## Installation
Start Flask and go to your localhost.

## Support
no

## Roadmap
I don't think we're updating this at all lmao.

## Authors and acknowledgment
Shoutout to my Scrum-Lord A1C Giblin. He's the G.O.A.T. fr fr
Amn Mitchell is also pretty poggers ngl.
Giblin:
    <3 Fratu fr fr


## License
Take it please, we don't want this

## Run Flask Debug Command
py -m flask --debug run